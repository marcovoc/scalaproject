import java.io.File
import java.lang.reflect.{Field, Method}
import java.text.SimpleDateFormat

object Main extends App {
  import spray.json._
  import JsonProtocol._
  import org.apache.spark.SparkContext
  import org.apache.spark.SparkConf
  import scala.reflect.runtime.universe._
  import scala.reflect.runtime.{universe=>ru}
  import org.apache.log4j.Logger
  import org.apache.log4j.Level

  if (args.length == 0) {
    println("need at least one parameter")
  }
  else
  {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)
    val conf = new SparkConf().setAppName("stat").setMaster("local")
    val sc = new SparkContext(conf)
    try {


      val folder = "/home/pineau/Documents/scala/hdfs sim/*"

      val reportsJson = sc.textFile(args(0)+"*")
      val reports = reportsJson.map(x => x.parseJson.convertTo[Report])
      val reportsByState = reports.groupBy(_.registrationState)
      val reportByVehicleColor = reports.groupBy(_.vehicleColor)
      val reportByDrone = reports.groupBy(_.droneId)
      println("report count by state: ")
      reportsByState.foreach(x => println("\t" + x._1 + " : " + x._2.size))
      println("report count by vehicle color: ")
      reportByVehicleColor.foreach(x => println("\t" + x._1 + " : " + x._2.size))
      println("report count by drone id: ")
      reportByDrone.foreach(x => println("\t" + x._1.get + " : " + x._2.size))
    }
    finally
      {
        sc.stop()
      }
  }


}