import java.io.File
import java.text.SimpleDateFormat

object Main extends App {
  import spray.json._
  import JsonProtocol._
  import java.util.Properties
  import org.apache.kafka.clients.producer._
  import scala.util.Try

  def getListOfFiles(dir: String):List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).filter(_.getName.endsWith(".csv")).map(_.getPath).toList
    } else {
      List[String]()
    }
  }

  def writeToKafka(value: String): Unit = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    val producer = new KafkaProducer[String, String](props)
    val record = new ProducerRecord[String, String]("test", "", value)
    producer.send(record)
    producer.close()
  }

  def parseCsvLine(line: String): String =
    {
          val cols = line.split(",").map(_.trim)
          val dateFormat = new SimpleDateFormat("MM/dd/yyyy")
          val date = dateFormat.parse(cols(4))
          val report = new Report(None, cols(1), cols(2), date.getTime, Try(cols(5).toInt).toOption, cols(24), Try(cols(23).toInt).toOption, cols(33))
          "<report>"+report.toJson.compactPrint
    }

  def processCsvFile(filePath: String) =
    {
      val bufferedSource = io.Source.fromFile(filePath)
      val lines = bufferedSource.getLines()
      lines.drop(1)
      lines.foreach(x => writeToKafka(parseCsvLine(x)))
    }

  if (args.length == 0) {
    println("need at least one parameter")
  }
  else
    {
      getListOfFiles(args(0)).foreach(x => processCsvFile(x))
    }


}