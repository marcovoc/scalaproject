#!/bin/bash
gnome-terminal -x bash -c "kafka_2.12-2.3.0/bin/zookeeper-server-start.sh kafka_2.12-2.3.0/config/zookeeper.properties"
sleep 2
gnome-terminal -x bash -c "kafka_2.12-2.3.0/bin/kafka-server-start.sh kafka_2.12-2.3.0/config/server.properties"
sleep 4
gnome-terminal -x bash -c "scala simdrone/target/scala-2.12/Simdrone-assembly-1.0.jar"
gnome-terminal -x bash -c "scala csv\ reader/target/scala-2.12/CsvReader-assembly-1.0.jar"