#!/bin/bash
gnome-terminal --title=zookeeper -x bash -c "kafka_2.12-2.3.0/bin/zookeeper-server-start.sh kafka_2.12-2.3.0/config/zookeeper.properties"
sleep 4
gnome-terminal --title=kafka -x bash -c "kafka_2.12-2.3.0/bin/kafka-server-start.sh kafka_2.12-2.3.0/config/server.properties"
sleep 5
#kafka_2.12-2.3.0/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test
gnome-terminal --title=Simdrone -x bash -c "scala simdrone/target/scala-2.12/Simdrone-assembly-1.0.jar"
gnome-terminal --title=CsvReader -x bash -c "scala csv\ reader/target/scala-2.12/CsvReader-assembly-1.0.jar" "/media/pineau/8AD03337D0332939/Users/laptop-masca/Downloads/nyc-parking-tickets/"
gnome-terminal --title=Consumer --active -x bash -c "kafka_2.12-2.3.0/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning"