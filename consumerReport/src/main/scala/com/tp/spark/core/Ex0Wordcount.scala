package com.tp.spark.core



import java.util
import org.apache.kafka.clients.consumer.KafkaConsumer
import java.util.Properties
import scala.collection.JavaConverters._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD



object Main extends App {

  def consumeFromKafka(topic: String) = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("auto.offset.reset", "latest")
    props.put("group.id", "consumer-group")
    val consumer: KafkaConsumer[String, String] = new KafkaConsumer[String, String](props)
    consumer.subscribe(util.Arrays.asList(topic))
    val t = new java.util.Timer()
    val taskScan = new java.util.TimerTask {
      def run() = scanOnce(consumer)
    }
    t.schedule(taskScan, 30000L, 30000L)
    scala.io.StdIn.readLine()
    taskScan.cancel()
  }

  def scanOnce(consumer: KafkaConsumer[String, String]) = {
    val conf = new SparkConf()
      .setAppName("Wordcount")
      .setMaster("local[*]") // here local mode. And * means you will use as much as you have cores.

    val sc = SparkContext.getOrCreate(conf)
    sc.parallelize(
      consumer
      .poll(1000)
      .asScala
      .iterator
      .filter(x => "<report>".r.findFirstIn(x.value()) != None )
      .map(x => x.value().substring(8))
      .toSeq)
      .saveAsTextFile("save/" + System.currentTimeMillis().toString())

    println("Reports saved")
  }



  consumeFromKafka("test")
}
