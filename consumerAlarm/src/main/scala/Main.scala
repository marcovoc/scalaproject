

object Main extends App {

  import java.util
  import org.apache.kafka.clients.consumer.KafkaConsumer
  import java.util.Properties
  import scala.collection.JavaConverters._


  def consumeFromKafka(topic: String) = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("auto.offset.reset", "latest")
    props.put("group.id", "consumer-group")
    val consumer: KafkaConsumer[String, String] = new KafkaConsumer[String, String](props)
    consumer.subscribe(util.Arrays.asList(topic))
    scanForever(consumer)
  }


  def scanForever(consumer: KafkaConsumer[String, String]): Void = {
    consumer
      .poll(1000)
      .asScala
      .filter(x => "<alarm>".r.findFirstIn(x.value()) != None)
      .map(x => x.value().substring(8))
      .foreach(println(_))
    scanForever(consumer)
  }


  consumeFromKafka("test")
}