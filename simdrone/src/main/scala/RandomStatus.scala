import java.util.Calendar

class RandomStatus {
  def generateRandomStatus(droneId: Int) : Status =
  {
    val r = scala.util.Random
    new Status(droneId, generateDate(), generateBatteryLevel(r))
  }
  private def generateDate(): Long =
  {
    Calendar.getInstance().getTime().getTime
  }
  private def generateBatteryLevel(r: scala.util.Random): Int =
  {
    r.nextInt(100)
  }
}
