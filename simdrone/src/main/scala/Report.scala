case class Report(
                       droneId: Option[Int],
                       plateId: String,
                       registrationState: String,
                       issueDate: Long,
                       violationCode: Option[Int],
                       streetName: String,
                       houseNumber: Option[Int],
                       val vehicleColor: String
                     )