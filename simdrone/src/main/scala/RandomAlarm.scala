import java.util.Calendar

class RandomAlarm {
  val alarmType = "motor problem" :: "Undefined vehicle infraction" :: Nil
  def generateRandomAlarm(droneId: Int) : Alarm =
  {
    val r = scala.util.Random
    Alarm(droneId, generateDate(), generateType(r))
  }
  private def generateDate(): Long =
  {
    Calendar.getInstance().getTime().getTime
  }
  private def generateType(r: scala.util.Random): String =
  {
    alarmType(r.nextInt(alarmType.length))
  }
}
