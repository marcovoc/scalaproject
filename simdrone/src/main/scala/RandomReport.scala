import java.text.SimpleDateFormat
import java.util.Calendar

class RandomReport {
  val country = "NY" :: "NJ" :: "MD" :: "NC" :: "LA" :: "IL" :: "PA" :: "ID" :: "ME" :: "CT" :: "VA" :: "NB" :: "SC" :: "NH" :: "NE" :: "IN" :: "MA" :: "OH" :: "TN" :: "CA" :: "GA" :: "FL" :: "TX" :: "MI" :: "AL" :: "CO" :: "RI" :: "OK" :: "ON" :: "MS" :: "AZ" :: "NM" :: "WI" :: "IA" :: "DC" :: "WV" :: "MN" :: "MO" :: "DE" :: "VT" :: "KY" :: "KS" :: "OR" :: "GV" :: "QB" :: "HI" :: "MT" :: "AK" :: "NV" :: "WA" :: "UT" :: "AR" :: "BC" :: "ND" :: "SK" :: "SD" :: "NS" :: "PR" :: "MX" :: "WY" :: "AB" :: "PE" :: "DP" :: "MB" :: "FO" :: "NT" :: "YT" :: "NF" :: Nil
  val roadName = "Gorge street" :: "Mickael Avenue" :: "Roger road" :: "Cinnamon street" :: Nil
  val colorVehicle = "Blue" :: "Red" :: "Yellow" :: "Green" :: "Cyan" :: "Orange" :: "Purple" :: Nil

  def generateRandomReportData(droneId: Int) : Report =
    {
      val r = scala.util.Random
      new Report(Some(droneId), generatePlateId(r), generateState(r), generateDate(), generateViolationCode(r),
        generateRoadName(r), generateHouseNumber(r), generateColorVehicle(r))
    }
  private def generatePlateId(r: scala.util.Random): String =
    {
      r.alphanumeric.take(3).mkString
    }
  private def generateState(r: scala.util.Random): String =
    {
      country(r.nextInt(country.length))
    }
  private def generateDate(): Long =
    {
      Calendar.getInstance().getTime().getTime
    }
  private def generateViolationCode(r: scala.util.Random): Option[Int] =
    {
      Some(r.nextInt(100))
    }
  private def generateRoadName(r : scala.util.Random): String =
    {
      roadName(r.nextInt(roadName.length))
    }
  private def generateHouseNumber(r : scala.util.Random) : Option[Int] =
    {
      Some(r.nextInt(100))
    }
  private def generateColorVehicle(r : scala.util.Random) : String =
    {
      colorVehicle(r.nextInt(colorVehicle.length))
    }
}
