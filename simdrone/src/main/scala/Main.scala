object Main extends App {
  import java.io._
  import spray.json._
  import JsonProtocol._
  import java.util.Properties
  import org.apache.kafka.clients.producer._

  def randomDroneId(): Int =
    {
      val r = scala.util.Random
      droneIds(r.nextInt(droneIds.length))
    }

  def isRandomAlert(): Boolean =
    {
      val r = scala.util.Random
      r.nextInt() < 20
    }

  def writeToKafka(value: String): Unit = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    val producer = new KafkaProducer[String, String](props)
    val record = new ProducerRecord[String, String]("test", "", value)
    producer.send(record)
    producer.close()
  }

  def randomReportTask() =
    {
      val randomizer = new RandomReport();
      val report = randomizer.generateRandomReportData(randomDroneId())
      val string = "<report>"+report.toJson.compactPrint
      println("sended :" + string)
      writeToKafka(string)
    }

  def randomAlarmTask() =
  {
    if(isRandomAlert())
    {
      val randomizer = new RandomAlarm();
      val report = randomizer.generateRandomAlarm(randomDroneId())
      val string = "<alarm>" + report.toJson.compactPrint
      println("sended :" + string)
      writeToKafka(string)
    }
  }

  def randomStatusTask(droneId: Int) =
  {
    val randomizer = new RandomStatus();
    val report = randomizer.generateRandomStatus(droneId)
    val string = "<status>"+report.toJson.compactPrint
    println("sended :" + string)
    writeToKafka(string)
  }


  val droneIds = 1 :: 2 :: 3 :: 4 :: 5 :: Nil
  val t = new java.util.Timer()
  val taskReport = new java.util.TimerTask {
    def run() = randomReportTask()
  }
  val taskStatus = new java.util.TimerTask {
    def run() = droneIds.foreach(x => randomStatusTask(x))
  }
  val taskAlarm = new java.util.TimerTask {
    def run() = randomAlarmTask()
  }
  t.schedule(taskReport, 5000L, 5000L)
  t.schedule(taskStatus, 1000L, 1000L)
  t.schedule(taskAlarm, 6000L, 6000L)
  scala.io.StdIn.readLine()
  taskStatus.cancel()
  taskReport.cancel()
  taskAlarm.cancel()
}