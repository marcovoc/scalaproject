import spray.json._
import DefaultJsonProtocol._

object JsonProtocol extends DefaultJsonProtocol
{
  implicit val reportDataFormat = jsonFormat8(Report)
  implicit val statusFormat = jsonFormat3(Status)
  implicit  val alarmFormat = jsonFormat3(Alarm)
}
